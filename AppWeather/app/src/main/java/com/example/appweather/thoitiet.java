package com.example.appweather;

/**
 * Created by Nguyễn Văn Bình on 3/11/2019.
 */

public class thoitiet {
    public String Day;
    public String trangthai;
    public String Image;
    public String Max;
    public String Min;

    public thoitiet(String day, String trangthai, String image, String max, String min) {
        Day = day;
        this.trangthai = trangthai;
        Image = image;
        Max = max;
        Min = min;
    }
}
