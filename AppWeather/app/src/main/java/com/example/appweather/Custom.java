package com.example.appweather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Custom extends BaseAdapter {
    Context context;
    ArrayList<thoitiet> arrayList;

    public Custom(Context context, ArrayList<thoitiet> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.dong, null);
        thoitiet thoitiet = arrayList.get(i);

        TextView txtday = view.findViewById(R.id.textviewngaythang);
        TextView txttrangthai = view.findViewById(R.id.tvStatus);
        TextView txtmax = view.findViewById(R.id.textviewmax);
        TextView txtmin = view.findViewById(R.id.textviewmin);
        ImageView imgtrnathai = view.findViewById(R.id.imagetrangthai);

        txtday.setText(thoitiet.Day);
        txttrangthai.setText(thoitiet.trangthai);
        txtmax.setText(thoitiet.Max + "°C");
        txtmin.setText(thoitiet.Min + "°C");

        Picasso.with(context).load("http://openweathermap.org/img/w/" + thoitiet.Image + ".png").into(imgtrnathai);

        return view;
    }
}
