package com.example.appweather.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appweather.R;
import com.example.appweather.TinTuc;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter_lvtintuc extends BaseAdapter {
    ArrayList<TinTuc> arrayListtintuc;
    Context context;

    public CustomAdapter_lvtintuc(Context context, ArrayList<TinTuc> arrayListtintuc) {
        this.arrayListtintuc = arrayListtintuc;
        this.context = context;
    }


    @Override
    public int getCount() {
        return arrayListtintuc.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListtintuc.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.dong_lvttt, null);
        TextView tvttt = convertView.findViewById(R.id.tvtt);
        ImageView imgicon = convertView.findViewById(R.id.imgIcon);
        TinTuc tinTuc = (TinTuc) getItem(position);
        tvttt.setText(tinTuc.tenTinTuc);
        Picasso.with(context).load(tinTuc.hinhAnh).into(imgicon);
        return convertView;
    }
}
