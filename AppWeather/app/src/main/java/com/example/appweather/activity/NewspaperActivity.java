package com.example.appweather.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appweather.R;
import com.example.appweather.TinTuc;
import com.example.appweather.adapter.CustomAdapter_lvtintuc;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class NewspaperActivity extends AppCompatActivity {
    ImageView imgViewBack;
    Button btntintuc;
    CustomAdapter_lvtintuc customAdapter_lvtintuc;
    ListView listView;
    String url = "http://www.nchmfnews.com/search/label/Th%E1%BB%9Di%20Ti%E1%BA%BFt%2024H/";
    ArrayList<TinTuc> arrayLists;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen3);
        listView = findViewById(R.id.listview);
        arrayLists = new ArrayList<>();
        initView();

        imgViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String ten = "";
                String hinhanh = "";
                Document document = Jsoup.parse(response);
                if (document != null) {
                    Elements elements = document.select("div.post-summary");
                    for (Element element : elements) {
                        Element element1ten = element.getElementsByTag("h3").first();
                        Element element1hinhanh = element.getElementsByTag("img").first();
                        if (element1ten != null) {
                            ten = element1ten.text();
                        }
                        if (element1hinhanh != null) {
                            hinhanh = element1hinhanh.attr("src");
                        }
                        arrayLists.add(new TinTuc(ten, hinhanh));

                    }
                    customAdapter_lvtintuc = new CustomAdapter_lvtintuc(NewspaperActivity.this, arrayLists);
                    listView.setAdapter(customAdapter_lvtintuc);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    private void initView() {
        imgViewBack = findViewById(R.id.imgViewBack);
        listView = findViewById(R.id.listview);
        btntintuc = findViewById(R.id.btnNews);
        arrayLists = new ArrayList<TinTuc>();
        customAdapter_lvtintuc = new CustomAdapter_lvtintuc(NewspaperActivity.this, arrayLists);
        listView.setAdapter(customAdapter_lvtintuc);


    }
}
