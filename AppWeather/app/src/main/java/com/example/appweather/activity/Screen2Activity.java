package com.example.appweather.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appweather.Custom;
import com.example.appweather.R;
import com.example.appweather.thoitiet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Screen2Activity extends AppCompatActivity {
    String tentp = "";
    ImageView imgViewBack,imgMenu;
    TextView tvNameCountry2;
    ListView listview;
    Custom custom;
    ArrayList<thoitiet> mangthoitiet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        Anhxa();
        Intent intent = getIntent();
        String city = intent.getStringExtra("name");

        if (city.equals("")) {
            tentp = "hanoi";

            Get7DaysData(tentp);
        } else {
            tentp = city;
            Get7DaysData(tentp);

        }
        imgViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void Anhxa() {
        imgViewBack =  findViewById(R.id.imgViewBack);
        tvNameCountry2 =  findViewById(R.id.tvNameCountry2);
        imgMenu =  findViewById(R.id.imgMenu);
        listview = findViewById(R.id.listview);
        mangthoitiet = new ArrayList<thoitiet>();
        custom = new Custom(Screen2Activity.this, mangthoitiet);
        listview.setAdapter(custom);



    }

  private void Get7DaysData(String data) {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(Screen2Activity.this);

            String url = "http://api.openweathermap.org/data/2.5/forecast/daily?q="+data+"&units=metric&cnt=7&APPID=50c4705f25a93cb530a067bd324c4c8d";
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject jsonObjectcity = jsonObject.getJSONObject("city");
                        String name = jsonObjectcity.getString("name");
                        tvNameCountry2.setText(name);

                        JSONArray jsonArraylist = jsonObject.getJSONArray("list");

                        for (int i = 0 ; i < jsonArraylist.length();i++){
                            JSONObject jsonObjectlist = jsonArraylist.getJSONObject(i);
                            String ngay = jsonObjectlist.getString("dt");

                            long l = Long.valueOf(ngay);
                            Date date = new Date(l*1000L);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE dd-MM-yyyy ");
                            String d = simpleDateFormat.format(date);

//
                            JSONObject jsonObjecttemp = jsonObjectlist.getJSONObject("temp");
                            String max = jsonObjecttemp.getString("max");
                            String min = jsonObjecttemp.getString("min");

                            Double a = Double.valueOf(max);
                            Double b = Double.valueOf(min);
                            String Nhietdomax = String.valueOf(a.intValue());
                            String Nhietdomin = String.valueOf(b.intValue());

                            JSONArray jsonArrayweather = jsonObjectlist.getJSONArray("weather");
                            JSONObject jsonObjectweather = jsonArrayweather.getJSONObject(0);
                            String status = jsonObjectweather.getString("description");
                            String icon = jsonObjectweather.getString("icon");

                            mangthoitiet.add(new thoitiet(d,status,icon,Nhietdomax,Nhietdomin));

                        }
                        custom.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           // android.widget.Toast.makeText(thoitiet.this, "Không tìm thấy tên thành phố cần tìm", Toast.LENGTH_SHORT).show();

                        }
                    });
            requestQueue.add(stringRequest);

        }
    }
    }
